# MCS Script
MCSScript is a Domain Specific Language for scripting communication between MCS and the satellite. It is powered by Groovy and runs on JVM, so all the Java libraries can be used.

## Quickstart

    state(TestState) {
        println "I'm a state in my own thread"
        sleep(1000)
        println "This state is now over."
    }
    
    State.start TestState

## Architecture 

## Syntax
http://www.groovy-lang.org/syntax.html

## MCSScript specific syntax
MCSScript uses some syntactic sugar (implemented via Groovy ASTTransformers) to achieve less verbose code at some places.

The most prominent syntax shortcut is the ```state``` declaration:

    state(TestState) {
        println "Hello"
    }
    
The MCSScript compiler will automatically transform this code into a Class:

    class TestState extends State {
        def run() {
            println "Hello"
        }
    }
which is a normal class declaration in Groovy. State declaration can be used as a way to create a simple state and works fine for most purposes.
