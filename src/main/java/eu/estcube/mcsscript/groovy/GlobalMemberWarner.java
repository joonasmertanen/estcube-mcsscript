package eu.estcube.mcsscript.groovy;

import eu.estcube.mcsscript.base.GlobalMember;
import org.codehaus.groovy.ast.*;
import org.codehaus.groovy.ast.expr.DeclarationExpression;
import org.codehaus.groovy.ast.expr.FieldExpression;
import org.codehaus.groovy.ast.stmt.BlockStatement;
import org.codehaus.groovy.control.CompilePhase;
import org.codehaus.groovy.control.SourceUnit;
import org.codehaus.groovy.control.messages.LocatedMessage;
import org.codehaus.groovy.control.messages.WarningMessage;
import org.codehaus.groovy.syntax.CSTNode;
import org.codehaus.groovy.syntax.Token;
import org.codehaus.groovy.transform.ASTTransformation;
import org.codehaus.groovy.transform.GroovyASTTransformation;

import java.util.List;

/**
 * Groovy's support for accessing top-level fields and methods from inner classes (ie. states) in script
 * is lacking and causes compile time problems. The error message given by the compiler is a bit unclear however,
 * so we run a ASTTransformer that does not actually transform anything, but analyzes the script for top level
 * member definitions, and tells the user what to use instead.
 *
 * TODO somehow make this emit warnings instead of errors. (how to get warnings from GroovyShell?)
 *
 * TODO better approach would be to use the ASTTransforming capabilities to automatically move global declarations
 * into a subclass  of their own and make references to those fields/methods use the subclass field/method.
 * Maybe the members won't even have to be moved if they're made static and somehow resolved from subclasses.
 */
@GroovyASTTransformation(phase = CompilePhase.SEMANTIC_ANALYSIS)
public class GlobalMemberWarner implements ASTTransformation {
    private static final String ERRORMSG_IGNORE = "You can also skip this error by prefixing the declaration " +
            "with @GlobalMember annotation";

    private static final String ERRORMSG_FIELD = "Top-level variables are not visible to states. Consider " +
            "moving the variable into a class and making it static. " + ERRORMSG_IGNORE;
    private static final String ERRORMSG_METHOD = "Top-level methods are not visible to states. Consider " +
            "moving the method into a class and making it static. " + ERRORMSG_IGNORE;

    private static final ClassNode CLASSNODE_GLOBAL = ClassHelper.make(GlobalMember.class);

    @Override
    public void visit(ASTNode[] astNodes, final SourceUnit sourceUnit) {
        sourceUnit.getAST().getStatementBlock().visit(new CodeVisitorSupport() {
            @Override
            public void visitDeclarationExpression(DeclarationExpression expression) {
                if (expression.getAnnotations(CLASSNODE_GLOBAL).size() > 0) return;

                Token t = Token.newString(expression.getText(), expression.getLineNumber(), expression.getColumnNumber());
                sourceUnit.getErrorCollector().addError(ERRORMSG_FIELD, t, sourceUnit);
                super.visitDeclarationExpression(expression);
            }
        });

        for (MethodNode mn : sourceUnit.getAST().getMethods()) {
            if (mn.getAnnotations(CLASSNODE_GLOBAL).size() > 0) return;

            Token t = Token.newString(mn.getText(), mn.getLineNumber(), mn.getColumnNumber());
            sourceUnit.getErrorCollector().addError(ERRORMSG_METHOD, t, sourceUnit);
        }
    }
}
