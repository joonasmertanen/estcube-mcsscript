package eu.estcube.mcsscript.groovy;

import groovy.lang.Closure;
import org.jdeferred.DoneCallback;
import org.jdeferred.Promise;
import org.jdeferred.impl.DefaultDeferredManager;

import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by test on 17.06.2015.
 */
public class PromiseAdapter {
    private final Promise p;

    public PromiseAdapter(Promise p) {
        this.p = p;
    }

    public PromiseAdapter then(final Closure onDone) {
        return new PromiseAdapter(p.then(new DoneCallback() {
            @Override
            public void onDone(Object o) {
                ClosureUtils.safeCallWithParams(onDone, o);
            }
        }));
    }

    public Object waitForResolve() {
        try {
            p.waitSafely();
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        // help
        final AtomicReference<Object> ref = new AtomicReference<Object>();
        p.then(new DoneCallback() {
            public void onDone(Object o) {
                ref.set(o);
            }
        });

        try {
            p.waitSafely();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }

        if (p.isResolved()) {
            return ref.get();
        }
        return null;
    }

    public static PromiseAdapter all(PromiseAdapter... adapters) {
        Promise[] promiseObjs = new Promise[adapters.length];
        for (int i = 0;i < adapters.length; i++) promiseObjs[i] = adapters[i].p;

        return new PromiseAdapter(new DefaultDeferredManager().when(promiseObjs));
    }
}
