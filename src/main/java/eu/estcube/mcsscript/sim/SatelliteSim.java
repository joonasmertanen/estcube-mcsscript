package eu.estcube.mcsscript.sim;

import java.util.concurrent.TimeoutException;

/**
 * A simple singleton file meant for early stage debugging
 */
public class SatelliteSim {
    private static volatile int callno = 0;
    public static Object[] send(String op, Object... params) throws TimeoutException {
        double batteryLevel = 3.0 + (++callno)*0.2;
        return new Object[] {batteryLevel, batteryLevel};
    }
}
