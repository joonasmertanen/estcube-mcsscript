package eu.estcube.mcsscript.base;

import com.google.common.collect.ClassToInstanceMap;
import com.google.common.collect.MutableClassToInstanceMap;
import eu.estcube.mcsscript.groovy.ClosureUtils;
import eu.estcube.mcsscript.groovy.PromiseAdapter;
import eu.estcube.mcsscript.sim.SatelliteSim;
import groovy.lang.Closure;
import groovy.lang.DelegatesTo;
import org.jdeferred.AlwaysCallback;
import org.jdeferred.Deferred;
import org.jdeferred.DoneCallback;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.lang.ref.Reference;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by test on 16.06.2015.
 */
public abstract class State {
    public Object send(String name) { return send(name, null); }

    public Object send(String name, @DelegatesTo(PacketRef.class) Closure body) {
        PacketRef packetRef = new PacketRef(name);

        // Create handlers
        body.setDelegate(packetRef);
        body.call();

        // Send packet
        // ...

        // Block until reply or timeout
        try {
            Object[] reply = SatelliteSim.send(name);
            return packetRef.handleReply(reply);
        } catch (TimeoutException e) {
            return packetRef.handleError(e);
        }
    }

    public abstract Object run();

    public class PacketRef {
        private final String op;

        public PacketRef(String op) {
            this.op = op;
        }

        private List<PacketCallback> dataCallbacks = new ArrayList<PacketCallback>();

        public PacketRef on(Closure condition, Closure body) {
            dataCallbacks.add(new PacketCallback(condition, body));
            return this;
        }

        public PacketRef onElse(Closure body) {
            return this.on(ClosureUtils.alwaysTrueClosure, body);
        }

        private List<PacketCallback> errorCallbacks = new ArrayList<PacketCallback>();

        public PacketRef onError(Closure body) {
            errorCallbacks.add(new PacketCallback(ClosureUtils.alwaysTrueClosure, body));
            return this;
        }

        public Object handleReply(Object... params) {
            for (PacketCallback cb : dataCallbacks) {
                if (ClosureUtils.isTrue(ClosureUtils.safeCallWithParams(cb.condition, params))) {
                    return ClosureUtils.safeCallWithParams(cb.handler, params);
                }
            }
            System.err.println("Warning: unhandled reply to packet '" + op + "' in state '" + State.this.getClass().getSimpleName() + "' for parameters " + Arrays.toString(params));
            return null;
        }

        public Object handleError(Exception e) {
            for (PacketCallback cb : errorCallbacks) {
                Object[] varargs = new Object[]{e};
                if (ClosureUtils.isTrue(ClosureUtils.safeCallWithParams(cb.condition, varargs))) {
                    return ClosureUtils.safeCallWithParams(cb.handler, varargs);
                }
            }
            throw new RuntimeException("Script failed with unhandled error", e);
        }
    }

    private static Map<Class<? extends State>, PromiseAdapter> promises = new HashMap<Class<? extends State>, PromiseAdapter>();

    private static Object runSequentially(State state) {
        return state.run();
    }

    private static ExecutorService threadPool = Executors.newCachedThreadPool();
    private static void runInParallel(final State state, final Deferred deferred) {
        threadPool.submit(new Runnable() {
            public void run() {
                deferred.resolve(state.run());
            }
        });
    }
    private static PromiseAdapter runState(final Class<? extends State> cls) {
        PromiseAdapter p = null;

        // We want concurrent access to promises map, but we dont use concurrent maps because those would
        // technically allow two threads to instantiate a new state at the same time.
        // Instead we want to force sequential access to promises, which makes sure that only one instance of each state
        // can exist. Performance isn't too big of a problem, because synchronization is only done on state starting
        // methods.
        synchronized (promises) {
            p = promises.get(cls);
            if (p == null) {
                Deferred deferred = new DeferredObject();

                final State state;
                try {
                    state = cls.newInstance();
                    runInParallel(state, deferred);
                } catch (Exception e) {
                    deferred.reject(e);
                }

                // TODO this is kind of ugly
                deferred.always(new AlwaysCallback() {
                    public void onAlways(Promise.State state, Object o, Object o2) {
                        promises.remove(cls);
                    }
                });

                p = new PromiseAdapter(deferred.promise());
                promises.put(cls, p);
            }
        }
        return p;
    }

    public static Object start(Class<? extends State> cls) {
        PromiseAdapter p = runState(cls);
        return p.waitForResolve();
    }

    public static PromiseAdapter startParallel(Class<? extends State> cls) {
        return runState(cls);
    }
}
