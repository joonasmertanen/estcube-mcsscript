package eu.estcube.mcsscript.base;

import groovy.lang.Closure;

/**
 * Created by test on 16.06.2015.
 */
public class PacketCallback {
    public final Closure condition;
    public final Closure handler;

    public PacketCallback(Closure condition, Closure handler) {
        this.condition = condition;
        this.handler = handler;
    }
}
