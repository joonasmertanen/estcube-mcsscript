package eu.estcube.mcsscript.base;

import eu.estcube.mcsscript.groovy.SimpleStateTransformer;
import org.codehaus.groovy.transform.GroovyASTTransformationClass;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by test on 16.06.2015.
 */
@Retention(RetentionPolicy.SOURCE)
@GroovyASTTransformationClass(classes = {SimpleStateTransformer.class})
public @interface SimpleState {
}
