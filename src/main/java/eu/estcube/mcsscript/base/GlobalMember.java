package eu.estcube.mcsscript.base;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @see eu.estcube.mcsscript.groovy.GlobalMemberWarner
 */
@Retention(RetentionPolicy.SOURCE)
public @interface GlobalMember {
}
