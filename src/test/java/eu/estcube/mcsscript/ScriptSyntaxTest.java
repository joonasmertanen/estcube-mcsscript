package eu.estcube.mcsscript;

import junit.framework.Assert;
import junit.framework.TestCase;
import org.codehaus.groovy.control.MultipleCompilationErrorsException;
import org.codehaus.groovy.control.messages.Message;
import org.junit.Test;

/**
 * Created by test on 17.06.2015.
 */
public class ScriptSyntaxTest extends TestCase {

    private void runCodeAndReturnFirstErr(String code) {
        new Script(code).execute();
    }

    @Test(expected = MultipleCompilationErrorsException.class)
    public void testStateDeclaration() throws Exception {
        runCodeAndReturnFirstErr("state()");
    }
}